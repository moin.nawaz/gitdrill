1. Create the following directory structure. (Create empty files where necessary)

	$ mkdir hello
	$ cd hellomk
	$ mkdir five one
	$ cd five
	$ mkdir six
	$ cd six
	$ touch c.txt
	$ mkdir seven
	$ cd seven
	$ touch error.log

	$ cd ../../..
	$ cd one
	$ touch a.txt b.txt
	$ mkdir two
	$ cd two
	$ touch d.txt
	$ mkdir three
	$ cd three
	$ touch e.txt
	$ mkdir four
	$ cd four
	$ touch access.log


2. Delete all the files with the .log extension  

	$ cd five/six/seven
	$ rm -r *.log
	$ cd ../../..
	$ cd one/two/three/four
	$ rm -r *.log
	$ cd ../../../..

3. Add the following content to a.txt

	$ cd one
	$ echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.">>a.txt
	$ cd ..

4. Delete the directory named five.
	
	$ rm -r five





	


